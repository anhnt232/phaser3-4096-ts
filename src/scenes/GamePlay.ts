import 'phaser';
import { Scene } from 'phaser';
import CONFIG from "../../config";

const ROW = 0;
const COL = 1;

export default class GamePlay extends Phaser.Scene{
    gameOptions : any;
    gameConfig : any;
    fieldArray : any;
    fieldGroup : Phaser.GameObjects.Group;
    score : number;
    bestScore: number;
    scoreText : Phaser.GameObjects.BitmapText;
    bestScoreText : Phaser.GameObjects.BitmapText;
    canMove: boolean;
    moveSound: Phaser.Sound.BaseSound;
    growSound: Phaser.Sound.BaseSound;
    movingTiles : number;
    logo : Phaser.GameObjects.Sprite;

    constructor(){
        super("GamePlay");
        this.gameOptions = {
            tileSize: CONFIG.tileSize,
            tweenSpeed: CONFIG.tweenSpeed,
            tileSpacing: CONFIG.tileSpacing,
            localStorageName: CONFIG.localStorageName,
        }
    }

    preload(){
        this.load.image("spot", "assets/sprites/spot.png");
        this.load.image("gametitle", "assets/sprites/gametitle.png");
        this.load.image("restart", "assets/sprites/restart.png");
        this.load.image("scorepanel", "assets/sprites/scorepanel.png");
        this.load.image("scorelabels", "assets/sprites/scorelabels.png");
        this.load.image("logo", "assets/sprites/Banner_4069.png");
        this.load.image("howtoplay", "assets/sprites/howtoplay.png");
        this.load.image("gameover", "assets/sprites/GameOver_4069.png");
        this.load.spritesheet("tiles", "assets/sprites/Tiles_4069.png", {
            frameWidth: this.gameOptions.tileSize,
            frameHeight: this.gameOptions.tileSize
        });
        this.load.bitmapFont("font", "assets/fonts/font.png", "assets/fonts/font.fnt");
        this.load.audio("move", ["assets/sounds/move.ogg", "assets/sounds/move.mp3"]);
        this.load.audio("grow", ["assets/sounds/grow.ogg", "assets/sounds/grow.mp3"]);
    }
    create(){
        this.fieldArray = [];
        this.fieldGroup = this.add.group();
        this.score = 0;
        this.bestScore = localStorage.getItem(this.gameOptions.localStorageName) == null ? 0 : Number(localStorage.getItem(this.gameOptions.localStorageName));
        for(var i = 0; i < 4; i++){
            this.fieldArray[i] = [];
            for(var j = 0; j < 4; j++){
                var spot = this.add.sprite(this.tileDestination(j, COL), this.tileDestination(i, ROW), "spot")
                var tile = this.add.sprite(this.tileDestination(j, COL), this.tileDestination(i, ROW), "tiles");
                tile.alpha = 0;
                tile.visible = false;
                this.fieldGroup.add(tile);
                this.fieldArray[i][j] = {
                    tileValue: 0,
                    tileSprite: tile,
                    canUpgrade: true
                }
            }
        }
        var restartButton = this.add.sprite(this.tileDestination(3, COL), this.tileDestination(0, ROW) - 200, "restart");
        restartButton.setInteractive();
        restartButton.on("pointerdown", function(){
            this.scene.start("GamePlay");
        }, this)
        this.add.sprite(this.tileDestination(1, COL), this.tileDestination(0, ROW) - 200, "scorepanel");
        this.add.sprite(this.tileDestination(1, COL), this.tileDestination(0, ROW) - 270, "scorelabels");
        this.add.sprite(10, 50, "gametitle").setOrigin(0, 0);
        var howTo = this.add.sprite(this.scale.width, 50, "howtoplay");
        howTo.setOrigin(1, 0);

        this.logo = this.add.sprite(this.scale.width / 2, this.scale.height -100, "logo");
        this.logo.setOrigin(0.5, 1);
        this.logo.setScale(1.1);
        // this.displayGameOver();
       
        this.scoreText = this.add.bitmapText(this.tileDestination(0, COL) - 80, this.tileDestination(0, ROW) - 225, "font", "0");
        this.bestScoreText = this.add.bitmapText(this.tileDestination(2, COL) - 190, this.tileDestination(0, ROW) - 225, "font", this.bestScore.toString());
        this.input.keyboard.on("keydown", this.handleKey, this);
        this.canMove = false;
        this.addTile();
        this.addTile();
        this.input.on("pointerup", this.endSwipe, this);
        this.moveSound = this.sound.add("move");
        this.growSound = this.sound.add("grow");
    }
    endSwipe(e){
        var swipeTime = e.upTime - e.downTime;
        var swipe = new Phaser.Geom.Point(e.upX - e.downX, e.upY - e.downY);
        var swipeMagnitude = Phaser.Geom.Point.GetMagnitude(swipe);
        var swipeNormal = new Phaser.Geom.Point(swipe.x / swipeMagnitude, swipe.y / swipeMagnitude);
        const rr = 0.6;
        if(swipeMagnitude > 10 && swipeTime < 600 && (Math.abs(swipeNormal.y) > rr || Math.abs(swipeNormal.x) > rr)){
            var children = this.fieldGroup.getChildren() as any[];
            if(swipeNormal.x > rr) {
                for (var i = 0; i < children.length; i++){
                    children[i].depth = this.scale.width - children[i].x;
                }
                this.handleMove(0, 1);
            }
            if(swipeNormal.x < -rr) {
                for (var i = 0; i < children.length; i++){
                    children[i].depth = children[i].x;
                }
                this.handleMove(0, -1);
            }
            if(swipeNormal.y > rr) {
                for (var i = 0; i < children.length; i++){
                    children[i].depth = this.scale.height - children[i].y;
                }
                this.handleMove(1, 0);
            }
            if(swipeNormal.y < -rr) {
                for (var i = 0; i < children.length; i++){
                    children[i].depth = children[i].y;
                }
                this.handleMove(-1, 0);
            }
        }
    }
    addTile(){
        var emptyTiles = [];
        for(var i = 0; i < 4; i++){
            for(var j = 0; j < 4; j++){
                if(this.fieldArray[i][j].tileValue == 0){
                    emptyTiles.push({
                        row: i,
                        col: j
                    })
                }
            }
        }
        if(emptyTiles.length > 0){
            var chosenTile = Phaser.Utils.Array.GetRandom(emptyTiles);
            let idx = 0;
            // let idx = Phaser.Math.Between(0, 6);
            this.fieldArray[chosenTile.row][chosenTile.col].tileValue = idx+1;
            this.fieldArray[chosenTile.row][chosenTile.col].tileSprite.visible = true;
            this.fieldArray[chosenTile.row][chosenTile.col].tileSprite.setFrame(idx);
            
            this.tweens.add({
                targets: [this.fieldArray[chosenTile.row][chosenTile.col].tileSprite],
                alpha: 1,
                duration: this.gameOptions.tweenSpeed,
                onComplete: function(tween){
                    if(tween.parent instanceof Phaser.Tweens.TweenManager){
                        let v = tween.parent.scene as any;
                        v.canMove = true;
                        v.checkGameOver();
                    }
                    
                },
            });
        }
	}
    handleKey(e){
        if(this.canMove){
            var children = this.fieldGroup.getChildren() as any[];
            switch(e.code){
                case "KeyA":
                case "ArrowLeft":
                    for (var i = 0; i < children.length; i++){
                        children[i].depth = children[i].x;
                    }
                    this.handleMove(0, -1);
                    break;
                case "KeyD":
                case "ArrowRight":
                    for (var i = 0; i < children.length; i++){
                        children[i].depth = this.scale.width - children[i].x;
                    }
                    this.handleMove(0, 1);
                    break;
                case "KeyW":
                case "ArrowUp":
                    for (var i = 0; i < children.length; i++){
                        children[i].depth = children[i].y;
                    }
                    this.handleMove(-1, 0);
                    break;
                case "KeyS":
                case "ArrowDown":
                    for (var i = 0; i < children.length; i++){
                        children[i].depth = this.scale.height - children[i].y;
                    }
                    this.handleMove(1, 0);
                    break;
            }
        }
    }
    handleMove(deltaRow, deltaCol){
        this.canMove = false;
        var somethingMoved = false;
        this.movingTiles = 0;
        var moveScore = 0;
        for(var i = 0; i < 4; i++){
            for(var j = 0; j < 4; j++){
                var colToWatch = deltaCol == 1 ? (4 - 1) - j : j;
                var rowToWatch = deltaRow == 1 ? (4 - 1) - i : i;
                var tileValue = this.fieldArray[rowToWatch][colToWatch].tileValue;
                if(tileValue != 0){
                    var colSteps = deltaCol;
                    var rowSteps = deltaRow;
                    while(this.isInsideBoard(rowToWatch + rowSteps, colToWatch + colSteps) && this.fieldArray[rowToWatch + rowSteps][colToWatch + colSteps].tileValue == 0){
                        colSteps += deltaCol;
                        rowSteps += deltaRow;
                    }
                    if(this.isInsideBoard(rowToWatch + rowSteps, colToWatch + colSteps) && (this.fieldArray[rowToWatch + rowSteps][colToWatch + colSteps].tileValue == tileValue) && this.fieldArray[rowToWatch + rowSteps][colToWatch + colSteps].canUpgrade && this.fieldArray[rowToWatch][colToWatch].canUpgrade && tileValue < 12){
                        this.fieldArray[rowToWatch + rowSteps][colToWatch + colSteps].tileValue ++;
                        moveScore += Math.pow(2, this.fieldArray[rowToWatch + rowSteps][colToWatch + colSteps].tileValue);
                        // this.fieldArray[rowToWatch + rowSteps][colToWatch + colSteps].canUpgrade = false;
                        this.fieldArray[rowToWatch][colToWatch].tileValue = 0;
                        this.moveTile(this.fieldArray[rowToWatch][colToWatch], rowToWatch + rowSteps, colToWatch + colSteps, Math.abs(rowSteps + colSteps), true);
                        somethingMoved = true;
                    }
                    else{
                        colSteps = colSteps - deltaCol;
                        rowSteps = rowSteps - deltaRow;
                        if(colSteps != 0 || rowSteps != 0){
                            this.fieldArray[rowToWatch + rowSteps][colToWatch + colSteps].tileValue = tileValue;
                            this.fieldArray[rowToWatch][colToWatch].tileValue = 0;
                            this.moveTile(this.fieldArray[rowToWatch][colToWatch], rowToWatch + rowSteps, colToWatch + colSteps, Math.abs(rowSteps + colSteps), false);
                            somethingMoved = true;
                        }
                    }
                }
            }
        }
        if(!somethingMoved){
            this.canMove = true;
        }
        else{
            this.moveSound.play();
            this.score += moveScore;
            if(this.score > this.bestScore){
                this.bestScore = this.score;
                localStorage.setItem(this.gameOptions.localStorageName, this.bestScore.toString());
            }
        }
    }
    moveTile(tile, row, col, distance, changeNumber){
        this.movingTiles ++;
        this.tweens.add({
            targets: [tile.tileSprite],
            x: this.tileDestination(col, COL),
            y: this.tileDestination(row, ROW),
            duration: this.gameOptions.tweenSpeed * distance,
            onComplete: function(tween){
                if(tween.parent instanceof Phaser.Tweens.TweenManager){
                    let v = tween.parent.scene as any;
                    v.movingTiles --;
                    if(changeNumber){
                        v.transformTile(tile, row, col);
                    }
                    if(v.movingTiles == 0){
                        v.scoreText.text = v.score.toString();
                        v.bestScoreText.text = v.bestScore.toString();
                        v.resetTiles();
                        v.addTile();
                    }
                }
                
            }
        })
    }
    transformTile(tile, row, col){
        this.growSound.play();
        this.movingTiles ++;
        tile.tileSprite.setFrame(this.fieldArray[row][col].tileValue - 1);
        this.tweens.add({
            targets: [tile.tileSprite],
            scaleX: 1.1,
            scaleY: 1.1,
            duration: this.gameOptions.tweenSpeed,
            yoyo: true,
            repeat: 1,
            onComplete: function(tween){
                if(tween.parent instanceof Phaser.Tweens.TweenManager){
                    let v = tween.parent.scene as any;
                    v.movingTiles --;
                    if(v.movingTiles == 0){
                        v.scoreText.text = v.score.toString();
                        v.bestScoreText.text = v.bestScore.toString();
                        v.resetTiles();
                        v.addTile();
                    }
                }
                
            }
        })
    }
    resetTiles(){
        for(var i = 0; i < 4; i++){
            for(var j = 0; j < 4; j++){
                this.fieldArray[i][j].canUpgrade = true;
                this.fieldArray[i][j].tileSprite.x = this.tileDestination(j, COL);
                this.fieldArray[i][j].tileSprite.y = this.tileDestination(i, ROW);
                if(this.fieldArray[i][j].tileValue > 0){
                    this.fieldArray[i][j].tileSprite.alpha = 1;
                    this.fieldArray[i][j].tileSprite.visible = true;
                    this.fieldArray[i][j].tileSprite.setFrame(this.fieldArray[i][j].tileValue - 1);
                }
                else{
                    this.fieldArray[i][j].tileSprite.alpha = 0;
                    this.fieldArray[i][j].tileSprite.visible = false;
                }
            }
        }
    }
    isInsideBoard(row, col){
        return (row >= 0) && (col >= 0) && (row < 4) && (col < 4);
    }
    tileDestination(pos, axis){
        var offset = (axis == ROW) ? (this.scale.height - this.scale.width) / 2 : 0;
        return pos * (this.gameOptions.tileSize + this.gameOptions.tileSpacing) + this.gameOptions.tileSize / 2 + this.gameOptions.tileSpacing + offset;
    }

    checkGameOver(){
 
        // loop through the entire board
        for(var i = 0; i < 4; i++){
            for(var j = 0; j < 4; j++){
 
                // if there is an empty tile, it's not game over
                if(this.fieldArray[i][j].tileValue == 0){
                    return;
                }
 
                // if there are two vertical adjacent tiles with the same value, it's not game over
                if((i < 3) && this.fieldArray[i][j].tileValue == this.fieldArray[i + 1][j].tileValue){
                    return;
                }
 
                // if there are two horizontal adjacent tiles with the same value, it's not game over
                if((j < 3) && this.fieldArray[i][j].tileValue == this.fieldArray[i][j + 1].tileValue){
                    return
                }
            }
        }
 
        // ok, it's definitively game over :(
        // this.scene.start("GameOver");
        // alert("No more move, GAME OVER");
        this.displayGameOver();
    }

    displayGameOver(){
        this.logo.destroy();
        this.logo = this.add.sprite(this.scale.width / 2, this.scale.height - 50, "logo");
        this.logo.setOrigin(0.5, 1);
        this.logo.setScale(1.1);

        let gameover = this.add.sprite(this.scale.width / 2, this.scale.height -300, "gameover");
        gameover.setOrigin(0.5, 0.5);

        // this.add.text(this.scale.width / 2, this.scale.height - 300, "No more move, GAME OVER !!!", {
        //     fontFamily: "Arial",
        //     fontSize: "60px",
        //     align: "center",
        //     color: "orange"
        // })
        // .setOrigin(0.5, 0.5);
    }
};