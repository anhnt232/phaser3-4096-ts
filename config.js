
const CONFIG = {
    tileSize: 200,
    tweenSpeed: 50,
    tileSpacing: 20,
    localStorageName: "top4096score",

}

export default CONFIG

